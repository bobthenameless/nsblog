__author__ = 'bobthenameless'

from flask_sqlalchemy import SQLAlchemy
from flask import Flask
from datetime import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)

class Post (db.Model):
    __tablename__ = "posts"

    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(5000))
    title = db.Column(db.Text)
    author = db.Column(db.String(40))
    dateposted = db.Column(db.DateTime)
    slugline = db.Column(db.String(30))


    def __init__(self, body, title, author, slugline, dateposted=None):
        self.body = body
        self.title = title
        self.author = author
        if dateposted is None:
            dateposted = datetime.utcnow()
        self.dateposted = dateposted
        self.slugline = slugline

    def __repr__(self):
        return "<Post('%s','%s')>" % (self.title, self.author)

    def toDict(self):
        d = {
            'body': self.body,
            'title': self.title,
            'author': self.author,
            'dateposted': str(self.dateposted),
            'slugline': self.slugline,
            'id': str(self.id)
        }

        return d