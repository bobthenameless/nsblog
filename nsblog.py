from flask import Flask, render_template, url_for, request, jsonify, json
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from models import Post

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)

#region MODELS


#endregion

#region UTILITY
def do_post_add(p):
        db.session.add(p)


def ltodPosts(posts):
    d = {}
    for p in posts:
        d[str(p.id)] = p.toDict()
    return d


#region ROUTING
@app.route('/')
def hello_world():
    return render_template('index.html')

@app.route('/api/getblogposts/<int:numResults>', methods=['GET','POST'])
def get_posts(numResults):
    posts = Post.query.all()[:numResults]
    return jsonify(ltodPosts(posts))


@app.route('/api/getblogposts/id/<int:postid>')
def show_post(postid):
    post = Post.query.filter_by(id=postid)
    return jsonify(ltodPosts(post))

@app.route('/api/addpost',methods=['GET', 'POST'])
def add_post():
    if request.method == 'POST':
        p = json.loads(request.data)
        newpost = Post(**p)
        try:
            do_post_add(newpost)
            db.session.commit()
        except:
            db.session.rollback()
            raise
        return "ok", 200


    else:
        return "notok", 500

with app.test_request_context():
    print url_for('hello_world')
    print url_for('show_post', postid=1)
    print url_for('add_post')

#endregion
############################

if __name__ == '__main__':
    app.run(debug=True)
