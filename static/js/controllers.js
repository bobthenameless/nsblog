'use strict';

/* Controllers */
var namelessCtrls = angular.module('namelessCtrls', []);



namelessCtrls.controller('NamelessContentCtrl', ['$scope', 'Post',
    function($scope, Post) {
        $scope.posts = Post.query_n({numResults: 50});
}]);

namelessCtrls.controller('NamelessRecentCtrl', ['$scope', '$routeParams', 'Post',
    function($scope, $routeParams, Post){
        $scope.posts = Post.query_n({numResults: $routeParams.numResults});
}]);

namelessCtrls.controller('NamelessPostIdCtrl', ['$scope', '$routeParams', 'Post',
    function($scope, $routeParams, Post){
        $scope.posts = Post.query_by_id({id: $routeParams.id});
}]);

namelessCtrls.controller('NamelessAddPost', ['$scope', '$http',
    function($scope, $http){
        $scope.post = {};
        $scope.submit = function(){
            $scope.submittext = "SENT"
            $http({
                method: 'POST',
                url: 'api/addpost',
                data: $scope.post
            }).success(function(data, status, headers, config) {
                $scope.submittext = "SUCCESS";
            }).error(function(data, status, headers, config) {
                $scope.submittext = "FAILED";

                });
        };
}]);
