"use strict";
var namelessServices = angular.module('namelessServices', ['ngResource']);

namelessServices.factory('Post', ['$resource',
    function($resource){
        var num = 5;
        var url = 'api/getblogposts/';
        return $resource(url, {}, {
                    query_n: {method:'GET', url: url+':numResults', params:{numResults:num}},
                    query_by_id: {method: 'GET', url: url+'id/:id', params:{id: 1}}
                });

    }]);

